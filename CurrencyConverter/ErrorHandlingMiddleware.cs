﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using CurrencyConverter;

namespace DynamicDocsCMIS.Utils
{
	/// <summary>
	/// Custom middleware for handling exceptions
	/// </summary>
	public class ErrorHandlingMiddleware
	{
		#region Data members

		private readonly RequestDelegate _next;		

		#endregion

		#region Constructors

		/// <summary>
		/// Creates ErrorHandlingMiddleware
		/// </summary>
		/// <param name="next">Delegate contains request</param>
		/// <param name="logger">Instance of ILogger</param>
		public ErrorHandlingMiddleware(RequestDelegate next)
		{
			_next = next;
			
		}

		#endregion

		#region Private Methods

		private string FormatRequest(HttpRequest request)
		{ 
			return $"{request.Scheme} {request.Host}{request.Path} {request.QueryString}";
		}

		private async Task HandleExceptionAsync(HttpContext context, Exception ex)
		{
			var (responseBody, responseContentType) = GerErrorResponse(context.Request.Path, ex);
			await WriteResponse(context, responseContentType, responseBody);
		}	

		private static async Task WriteResponse(HttpContext context, string responseContentType,
			string responseBody)
		{
			if (!context.Response.HasStarted)
			{
				context.Response.StatusCode = 500;
				context.Response.ContentType = responseContentType;
				await context.Response.WriteAsync(responseBody);
			}
		}

		public static (string body, string contentType) GerErrorResponse(string path, Exception ex)
		{
			string responseBody;
			string responseContentType;			
			
				var errorModel = new ErrorMessageModel
				{					
					Message = ex.Message				
				};

				var json = JsonConvert.SerializeObject(errorModel);
				responseBody = json;
				responseContentType = "application/json";
		

			return (responseBody, responseContentType);
		}		

		#endregion

		/// <summary>
		/// Execute HTTP request
		/// </summary>
		/// <param name="context">Http Context</param>
		/// <returns>Result of request</returns>
		public async Task InvokeAsync(HttpContext context)
		{
			try
			{
				await _next(context);				
			}			
			catch (Exception ex) // буде працювати кожний раз коли в коді щось піде не так
			{
				await HandleExceptionAsync(context, ex);
			}
		}
	}
}
