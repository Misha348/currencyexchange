﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter
{
	public class ErrorMessageModel
	{
		public string Cause { get; set; }
		public string Content { get; set; }
		public string Name { get; set; }
		public string Message { get; set; }
		public string Intent { get; set; }
	}
}
