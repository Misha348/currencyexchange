﻿using CurrencyConverter.Entities;
using CurrencyConverter.Models;
using CurrencyConverter.Models.ExchangeVM;
using CurrencyConverter.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;

namespace CurrencyConverter.Controllers
{
	enum CurrencyDataCondition
	{
		convertedFromCurrency,
		convertedToCurrency
	}

	public class ExchangeController : Controller
	{
		private readonly ICurrencyTypeService _currencyTypesService;
		private readonly ICurrencyExchangeService _currencyExchangeService;	
		public ExchangeController(ICurrencyTypeService currencyService, ICurrencyExchangeService currencyExchangeService)
		{
			_currencyTypesService = currencyService;
			_currencyExchangeService = currencyExchangeService;
		}

		public async Task<IActionResult> Index()
		{
			var twoCurrenciesList = _currencyTypesService.GetExchangeCurrenciesLists();

			ViewBag.ListOfCurrency_1 = twoCurrenciesList.First;
			ViewBag.ListOfCurrency_2 = twoCurrenciesList.Second;

			return await Task.Run(() => View());
		}

		public async Task<IActionResult> GetCurrencyTypesSelectionList()
		{
			var currencyTypesList = _currencyTypesService.GetAllCurrenciesLists();
			return await Task.Run(() => View(currencyTypesList));
		}

		[HttpPost]
		public async Task<IActionResult> SaveSelectedCurrencyForExchange(List<CurrencyTypesSelectionVM> selectedCurrencyList)
		{
			_currencyTypesService.UpdateSelectedCurrencyList(selectedCurrencyList);
			return await Task.Run(() => RedirectToAction("Index"));
		}


		[HttpPost]
		public JsonResult Index(string[] exchangeOperationData)
		{
			var style = NumberStyles.Number | NumberStyles.AllowCurrencySymbol;
			var culture = CultureInfo.CreateSpecificCulture("en-GB");

			var exchangeResultModel = new ExchangeOperationResultVM();
			if (Double.TryParse(exchangeOperationData[1], style, culture, out double firstAmount) &&
				Double.TryParse(exchangeOperationData[3], style, culture, out double secondAmount))
			{
				exchangeResultModel = _currencyExchangeService.GetCurrencyExchangeResultModel(exchangeOperationData);
				return Json(exchangeResultModel);
			}
			return Json("Bad responce");
		}

		public async Task<IActionResult> GetExchangesList(ExchangeOperationFilterVM filter, int page = 1)
		{
			var model = _currencyExchangeService.GetExchangeOperations(filter, page);

			ViewBag.StaticticModel_1 = _currencyExchangeService.GetCurrencyChartData(CurrencyDataCondition.convertedFromCurrency.ToString());
			ViewBag.StaticticModel_2 = _currencyExchangeService.GetCurrencyChartData(CurrencyDataCondition.convertedToCurrency.ToString());

			ViewBag.CurrentSortOrder = filter.SortOrder;  
			ViewData["Id_Sorting_Order"] = filter.SortOrder == "Id_asc" ? "Id_desc" : "Id_asc";
			ViewData["From_Amount_Sorting_Order"] = filter.SortOrder == "from_amount_asc" ? "from_amount_desc" : "from_amount_asc";
			ViewData["Converted_Amount_Sorting_Order"] = filter.SortOrder == "converted_amount_asc" ? "converted_amount_desc" : "converted_amount_asc";			
			ViewData["Date_Sorting_Order"] = filter.SortOrder == "date_order_asc" ? "date_order_desc" : "date_order_asc";

			return await Task.Run(() => View(model));
		}

		

		
	}
}
