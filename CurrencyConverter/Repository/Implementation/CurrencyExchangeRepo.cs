﻿using CurrencyConverter.Entities;
using CurrencyConverter.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Repository.Implementation
{
	public class CurrencyExchangeRepo: BaseRepository<ExchangeOperation, int>, ICurrencyExchangeRepo
	{
		public CurrencyExchangeRepo(AppDbContext context): base(context)
		{ 
		}
	}
}
