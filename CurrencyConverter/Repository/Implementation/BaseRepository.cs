﻿using CurrencyConverter.Entities.BaseEntity;
using CurrencyConverter.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CurrencyConverter.Repository.Implementation
{
	public abstract class BaseRepository<TEntity, TId> : IBaseRepositiry<TEntity, TId> where TEntity : class, IBaseEntity<TId>
	{
		public DbContext Context { get; }
		public DbSet<TEntity> DbSet { get; }

		public BaseRepository(DbContext context)
		{
			this.Context = context;
			this.DbSet = this.Context.Set<TEntity>();
		}

		public virtual IQueryable<TEntity> GetAll(/*bool notDeleted = true*/)
		{
			var query = DbSet.AsQueryable();
			//if (notDeleted)
			//	query = query.Where(x => x. != null);
			return DbSet.AsQueryable();
		}

		public virtual async Task<TId> Add(TEntity entity)
		{
			await this.DbSet.AddAsync(entity);
			this.Context.SaveChanges();
			return entity.Id;
		}

		public List<TEntity> GetPageList(int page, int pageSize = 2, Expression<Func<TEntity, bool>> filter = null)
		{
			int pageNo = page - 1;
			var query = this.GetAll();
			if (filter != null)
				query = query.Where(filter);
			var list = query.OrderBy(x => x.Id)
				.Skip(pageNo * pageSize)
				.Take(pageSize)
				.ToList();
			return list;
		}

		public void Update(TEntity entity)
		{
			DbSet.Update(entity).State = EntityState.Modified;
			this.Context.SaveChanges();
		}
	}
}
