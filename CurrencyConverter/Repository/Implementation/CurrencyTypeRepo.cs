﻿using CurrencyConverter.Repository.Interfaces;
using CurrencyConverter.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Repository.Implementation
{
	public class CurrencyTypeRepo : BaseRepository<CurrencyType, int>,  ICurrencyTypeRepo
	{
		public CurrencyTypeRepo(AppDbContext context): base(context)
		{
				
		}
	}
}
