﻿using CurrencyConverter.Entities.BaseEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CurrencyConverter.Repository.Interfaces
{
	public interface IBaseRepositiry<TEntity, TId> where TEntity : class, IBaseEntity<TId>
	{
		IQueryable<TEntity> GetAll(/*bool notDeleted = true*/);
		Task<TId> Add(TEntity entity);	
		List<TEntity> GetPageList(int page, int pageSize, Expression<Func<TEntity, bool>> filter = null);
		void Update(TEntity entity);
	}
}
