﻿using CurrencyConverter.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CurrencyConverter.Entities;

namespace CurrencyConverter.Repository.Interfaces
{
	public interface ICurrencyTypeRepo: IBaseRepositiry<CurrencyType, int>
	{
	}
}
