﻿using CurrencyConverter.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Repository.Interfaces
{
	public interface ICurrencyExchangeRepo: IBaseRepositiry<ExchangeOperation, int>
	{
	}
}
