﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models
{
	public class ExchangeOperationFilterVM
	{
		[Display(Name = "Id.")]
		public int Id { get; set; }

		[Display(Name = "Convert from: ")]
		[RegularExpression(@"[A-Z]{3}", ErrorMessage = "format: USD, GBP, EUR ...")]
		public string FromCurrency { get; set; }

		[Display(Name = "From amount:")]
		public decimal FromAmount { get; set; }

		[Display(Name = "Convert to:")]
		[RegularExpression(@"[A-Z]{3}", ErrorMessage = "format: USD, GBP, EUR ...")]
		public string ToCurrency { get; set; }

		[Display(Name = "To amount:")]
		public decimal ToAmount { get; set; }

		[Display(Name = "Search since:")]
		//[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
		[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public DateTime PublishTimeSince { get; set; } = DateTime.Now;

		[RegularExpression(@"^(3[01]|[12][0-9]|0[1-9])\.(1[0-2]|0[1-9])\.[0-9]{4}$",
		 ErrorMessage = "Correct date format: DD.MM.YYYY")]
		public string PublishTimeSinceField { get; set; }

		[Display(Name = "Search till:")]
		//[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
		[DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
		public DateTime PublishTimeTill { get; set; } = DateTime.Now;

		[RegularExpression(@"^(3[01]|[12][0-9]|0[1-9])\.(1[0-2]|0[1-9])\.[0-9]{4}$",
		ErrorMessage = "Correct date format: DD.MM.YYYY")]
		public string PublishTimeTillField { get; set; }
	
		public string SortOrder { get; set; }
		
	}
}
