﻿using CurrencyConverter.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models.ExchangeVM
{
	public class ExchangeOperationVM
	{
		public int CurrentPage { get; set; }
		public int MaxPage { get; set; }
		public List<ExchangeOperation> ExOpList { get; set; }
		public ExchangeOperationFilterVM ExchangeOperationsFilter { get; set; }
		public int Page { get; set; }
	}
}
