﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models
{
	public class TwoListsOfCurrenciesVM
	{
		public List<CurrencyTypeVM> First { get; set; }
		public List<CurrencyTypeVM> Second { get; set; }
	}
}
