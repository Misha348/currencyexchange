﻿using System.Globalization;

namespace CurrencyConverter.Models
{
	public class ExchangeOperationData
	{
		public string FirstCurrensyType {get; set;}
		public double FirstCurrensyTypeAmount { get; set; }
		public string SecondCurrensyType { get; set; }
		public double SecondCurrensyTypeAmount { get; set; }

		public ExchangeOperationData(string[] exchangeData)
		{
			FirstCurrensyType = exchangeData[0];
			FirstCurrensyTypeAmount = double.Parse(exchangeData[1], CultureInfo.InvariantCulture);
			SecondCurrensyType = exchangeData[2];
			SecondCurrensyTypeAmount = double.Parse(exchangeData[3], CultureInfo.InvariantCulture);
		}	
	}
}
