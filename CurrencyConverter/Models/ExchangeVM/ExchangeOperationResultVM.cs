﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models
{
	public class ExchangeOperationResultVM
	{
		public string CurrencyFromType { get; set; }
		public string CurrencyToType { get; set; }
		public decimal ConvertedAmountResult { get; set; }
		public decimal ExchangeCourse { get; set; }

	}
}
