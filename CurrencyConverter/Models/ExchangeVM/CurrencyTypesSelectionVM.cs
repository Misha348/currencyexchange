﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models.ExchangeVM
{
	public class CurrencyTypesSelectionVM
	{
		[Display(Name = "Id")]
		public int Id { get; set; }

		[Display(Name = "Currency")]
		public string Name { get; set; }

		[Display(Name = "For exchange")]
		public bool IsSelected { get; set; }
	}
}
