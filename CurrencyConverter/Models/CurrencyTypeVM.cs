﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models
{
	public class CurrencyTypeVM
	{
		public int Id { get; set; }	
		public string Name { get; set; }	

		[RegularExpression(@"^[0-9]+(?:\.[0-9]+)?$", ErrorMessage = "only positive real numbers")]		
		public string LeftCurrencyAmount { get; set; }

	
		[RegularExpression(@"^[0-9]+(?:\.[0-9]+)?$", ErrorMessage = "only positive real numbers")]
		public string RightCurrencyAmount { get; set; }
	}
}
