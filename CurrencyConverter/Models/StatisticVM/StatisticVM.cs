﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models.StatisticVM
{
	public class StatisticVM
	{
		public string Title { get; set; }
		public List<ChartDataItemVM> ListChartDataItems { get; set; }
	}
}
