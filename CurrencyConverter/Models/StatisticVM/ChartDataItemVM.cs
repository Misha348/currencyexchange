﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Models.StatisticVM
{
	public class ChartDataItemVM
	{
		public string SectorLabel { get; set; } // x scale
		public string SectorValue { get; set; } // y scale
		public string SectorColor { get; set; }
	}
}
