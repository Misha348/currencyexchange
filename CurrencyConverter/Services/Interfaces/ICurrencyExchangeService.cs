﻿using CurrencyConverter.Entities;
using CurrencyConverter.Models;
using CurrencyConverter.Models.ExchangeVM;
using CurrencyConverter.Models.StatisticVM;

namespace CurrencyConverter.Services.Interfaces
{
	public interface ICurrencyExchangeService
	{
		ExchangeOperationResultVM GetCurrencyExchangeResultModel(string [] exchangeDataItems);
		ExchangeOperationVM GetExchangeOperations(ExchangeOperationFilterVM exchangeOpFilter, /*string sortOrder,*/ int page);
		StatisticVM GetCurrencyChartData(string dataCondition);
		void AddExchangeOperation(ExchangeOperation newExchangeOp);

	}
}
