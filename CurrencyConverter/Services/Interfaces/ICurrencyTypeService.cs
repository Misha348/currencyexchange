﻿using CurrencyConverter.Entities;
using CurrencyConverter.Models;
using CurrencyConverter.Models.ExchangeVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Services.Interfaces
{
	public interface ICurrencyTypeService
	{
		List<CurrencyTypesSelectionVM> GetAllCurrenciesLists();
		TwoListsOfCurrenciesVM GetExchangeCurrenciesLists();
		void UpdateSelectedCurrencyList(List<CurrencyTypesSelectionVM> selectedCurrencyList);
	}
}
