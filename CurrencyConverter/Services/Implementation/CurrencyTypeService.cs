﻿using CurrencyConverter.Entities;
using CurrencyConverter.Models;
using CurrencyConverter.Models.ExchangeVM;
using CurrencyConverter.Repository.Interfaces;
using CurrencyConverter.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Services.Implementation
{
	public class CurrencyTypeService : ICurrencyTypeService
	{
		private readonly ICurrencyTypeRepo _currencyTypeRepo;
		public CurrencyTypeService(ICurrencyTypeRepo currencyTypeRepo)
		{
			_currencyTypeRepo = currencyTypeRepo;
		}

		public List<CurrencyTypesSelectionVM> GetAllCurrenciesLists()
		{
			var query = _currencyTypeRepo.GetAll().ToList();
			List<CurrencyTypesSelectionVM> list = query.Select(c =>
					new CurrencyTypesSelectionVM
					{
						Id = c.Id,
						Name = c.Name,
						IsSelected = c.IsSelected
					}).ToList();
			return list;
		}

		private List<CurrencyTypeVM> GetCurrencyList()
		{
			var query = _currencyTypeRepo.GetAll().ToList();

			List<CurrencyTypeVM> list = query.Select(c =>
			{
				if (c.IsSelected == true)
					return new CurrencyTypeVM { Id = c.Id, Name = c.Name };
				return null;
			}).ToList();
			list = list.Where(i => i != null).ToList();

			return list;
		}		

		public TwoListsOfCurrenciesVM GetExchangeCurrenciesLists()
		{
			TwoListsOfCurrenciesVM twoCurrenciesList = new TwoListsOfCurrenciesVM();
			twoCurrenciesList.First = GetCurrencyList();			
			twoCurrenciesList.Second = GetCurrencyList();		

			return twoCurrenciesList;
		}

		public void UpdateSelectedCurrencyList(List<CurrencyTypesSelectionVM> selectedCurrencyList)
		{
			var query = _currencyTypeRepo.GetAll().ToList();
			foreach (var item in query)
			{
				item.IsSelected = selectedCurrencyList.FirstOrDefault(sc => sc.Id == item.Id).IsSelected;				
				_currencyTypeRepo.Update(item);
			}		
		}
	}
}
