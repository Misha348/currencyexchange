﻿using CurrencyConverter.Entities;
using CurrencyConverter.Helpers;
using CurrencyConverter.Models;
using CurrencyConverter.Models.ExchangeVM;
using CurrencyConverter.Models.StatisticVM;
using CurrencyConverter.Repository.Interfaces;
using CurrencyConverter.Services.Interfaces;
using MoreLinq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CurrencyConverter.Services.Implementation
{
	public class CurrencyExchangeService: ICurrencyExchangeService
	{
		private readonly ICurrencyExchangeRepo _currencyExchangeRepo;
		private readonly IHttpClientFactory _httpClientFactory;
		public CurrencyExchangeService(ICurrencyExchangeRepo currencyExchangeRepo, IHttpClientFactory httpClientFactory)
		{
			_currencyExchangeRepo = currencyExchangeRepo;
			_httpClientFactory = httpClientFactory;
		}
		// in service we create exchangeOperation and path it to repo (_currencyExchangeRepo.Add(model))

		private List<KeyValuePair<string, double>> FilterUnnecassaryRates(ExchangeOperationData model, string ratesSet)
		{
			JObject json = (JObject)JsonConvert.DeserializeObject(ratesSet);
			JObject ratesAsJson = (JObject)json["rates"];

			var ratesAsString = ratesAsJson.ToString();
			Dictionary<string, double> allRates = JsonConvert.DeserializeObject<Dictionary<string, double>>(ratesAsString);

			// start
			List<KeyValuePair<string, double>> currentRates = new List<KeyValuePair<string, double>>();

			var currencyRatePair_1 = new KeyValuePair<string, double>(model.FirstCurrensyType, allRates[model.FirstCurrensyType]);
			var currencyRatePair_2 = new KeyValuePair<string, double>(model.SecondCurrensyType, allRates[model.SecondCurrensyType]);
			currentRates.Add(currencyRatePair_1);
			currentRates.Add(currencyRatePair_2);

			return currentRates;
		}

		private async Task<string> CurrencyRatesAPICallAsync()
		{
			var client = _httpClientFactory.CreateClient();
			var allRates = await client.GetStringAsync("https://api.exchangeratesapi.io/latest?base=USD");			
			return allRates;
		}
		 
		private List<KeyValuePair<string, double>> GetCurrencyRates(ExchangeOperationData exchangeDataModel)
		{			
			var allRates = CurrencyRatesAPICallAsync().Result;
			var currentExchangeRates = FilterUnnecassaryRates(exchangeDataModel, allRates);	
			return currentExchangeRates;			
		}

		public ExchangeOperationResultVM GetCurrencyExchangeResultModel(string[] exchangeDataItems)
		{
			//model for holding exchangeData(from, to, amount ...)
			var exchangeDataModel = new ExchangeOperationData(exchangeDataItems);
			// get rates for currencies for exchanges("currencyName - amountPerDollar")
			var currencyExchangeRates = GetCurrencyRates(exchangeDataModel);
			var currencyExchangeOperationModel = new ExchangeOperationResultVM();

			if (exchangeDataItems[1] == "0")
			{
				currencyExchangeOperationModel =
					new ExchangeOperationProviderForLeftValueToSearch().ProvideExchange(currencyExchangeRates, exchangeDataModel);
				var exchOp = new ExchangeOperation
				{
					FromCurrency = currencyExchangeOperationModel.CurrencyFromType,
					FromAmount = decimal.Parse(exchangeDataItems[3], CultureInfo.InvariantCulture),
					ToCurrency = currencyExchangeOperationModel.CurrencyToType,
					ToAmount = currencyExchangeOperationModel.ConvertedAmountResult,
					DateCreate = DateTime.Now
				};

				AddExchangeOperation(exchOp);
			}

			if (exchangeDataItems[3] == "0")
			{
				currencyExchangeOperationModel =
					new ExchangeOperationProviderForRightValueToSearch().ProvideExchange(currencyExchangeRates, exchangeDataModel);
				var exchOp = new ExchangeOperation()
				{
					FromCurrency = currencyExchangeOperationModel.CurrencyFromType,
					FromAmount = decimal.Parse(exchangeDataItems[1], CultureInfo.InvariantCulture),
					ToCurrency = currencyExchangeOperationModel.CurrencyToType,
					ToAmount = currencyExchangeOperationModel.ConvertedAmountResult,
					DateCreate = DateTime.Now
				};
				AddExchangeOperation(exchOp);
			}
			return currencyExchangeOperationModel;
		}


		// It's better to use ExchangeOperationAddVM model instead of direct using ExchangeOperation entity
		public void AddExchangeOperation(ExchangeOperation newExchangeOp)
		{
			_currencyExchangeRepo.Add(newExchangeOp);
		}
		
		public ExchangeOperationVM GetExchangeOperations(ExchangeOperationFilterVM filter,  int page)
		{
			var query = _currencyExchangeRepo.GetAll();
			ExchangeOperationVM model = new ExchangeOperationVM();			
			
			FilterManager filterManager = new FilterManager(query);
			query = filterManager.GetFilteredExchangeOperationsList(filter);

			int pageSize = 7;
			int pageNo = page - 1;
			SortingManager sortingManager = new SortingManager(query, pageSize, pageNo);
			model.ExOpList = sortingManager.GetSortedExchangeOperationList(filter.SortOrder);			

			int allCount = query.Count();
			model.Page = page;
			model.MaxPage = (int)Math.Ceiling((double)allCount / pageSize);
			model.ExchangeOperationsFilter = filter;

			return model;
		}

		public StatisticVM GetCurrencyChartData(string dataCondition)
		{
			var random = new Random();

			var query = _currencyExchangeRepo.GetAll().ToList();
			var exchOpCount = query.Count();

			StatisticVM statisticChartModel = new StatisticVM
			{
				Title = "Currency to exchange",
				ListChartDataItems = new List<ChartDataItemVM>()
			};

			List<string> cuurrTypes = query.Select(op =>
			{
				if (dataCondition == "convertedFromCurrency")
					return op.FromCurrency; 
				else if (dataCondition == "convertedToCurrency")
					return op.ToCurrency; 
				return "";
			}).Distinct().ToList();			
			
			foreach (var item in cuurrTypes)
			{
				var thisCurrTypeTotalCountFromAllExchanges = query.Where(c =>
				{
					if (dataCondition == "convertedFromCurrency")
						return c.FromCurrency == item;
					if (dataCondition == "convertedToCurrency")
						return c.ToCurrency == item;
					return false;
				}).Count();

				decimal thisCurrencyExchangesPercentFromAllExchanges = Math.Round((decimal)(thisCurrTypeTotalCountFromAllExchanges * 100 / exchOpCount), 2, MidpointRounding.AwayFromZero);
				var color = string.Format("#{0:X6}", random.Next(0x1000000));

				statisticChartModel.ListChartDataItems.Add(new ChartDataItemVM()
				{ 
					SectorLabel = item, 
					SectorValue = thisCurrencyExchangesPercentFromAllExchanges.ToString(), 
					SectorColor = color
				});
			}
			return statisticChartModel;
		}		
	}
}
