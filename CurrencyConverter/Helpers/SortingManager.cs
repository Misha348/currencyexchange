﻿using CurrencyConverter.Entities;
using CurrencyConverter.Models.ExchangeVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Helpers
{
	public enum SortOrderType
	{		
		Id_asc,  
		Id_desc,   
		from_amount_asc, 
		from_amount_desc, 
		date_order_asc,
		date_order_desc,
		converted_amount_asc, 
		converted_amount_desc,
	}

	public class SortingManager
	{
		public Dictionary<string, Func<List<ExchangeOperation>>> sortedList;
		
		public SortingManager(IQueryable<ExchangeOperation> query, int pageSize, int pageNo)
		{
			sortedList = new Dictionary<string, Func<List<ExchangeOperation>>> ();
			sortedList.Add("", () => { return query.OrderBy(x => x.Id).Skip(pageNo * pageSize).Take(pageSize).ToList(); });
			sortedList.Add(SortOrderType.Id_asc.ToString(), () => { return query.OrderBy(x => x.Id).Skip(pageNo * pageSize).Take(pageSize).ToList();  });
			sortedList.Add(SortOrderType.Id_desc.ToString(), () => { return query.OrderByDescending(x => x.Id).Skip(pageNo * pageSize).Take(pageSize).ToList(); });

			sortedList.Add(SortOrderType.from_amount_asc.ToString(), () => { return query.OrderBy(x => x.FromAmount).Skip(pageNo * pageSize).Take(pageSize).ToList(); });
			sortedList.Add(SortOrderType.from_amount_desc.ToString(), () => { return query.OrderByDescending(x => x.FromAmount).Skip(pageNo * pageSize).Take(pageSize).ToList(); });

			sortedList.Add(SortOrderType.date_order_asc.ToString(), () => { return query.OrderBy(x => x.DateCreate).Skip(pageNo * pageSize).Take(pageSize).ToList(); });
			sortedList.Add(SortOrderType.date_order_desc.ToString(), () => { return query.OrderByDescending(x => x.DateCreate).Skip(pageNo * pageSize).Take(pageSize).ToList(); });

			sortedList.Add(SortOrderType.converted_amount_asc.ToString(), () => { return query.OrderBy(x => x.ToAmount).Skip(pageNo * pageSize).Take(pageSize).ToList(); });
			sortedList.Add(SortOrderType.converted_amount_desc.ToString(), () => { return query.OrderByDescending(x => x.ToAmount).Skip(pageNo * pageSize).Take(pageSize).ToList(); });
		}

		public List<ExchangeOperation> GetSortedExchangeOperationList(string sortOrder)
		{
			if (sortOrder == null)
				sortOrder = "";
			return sortedList[sortOrder]();
		}
	}
}
