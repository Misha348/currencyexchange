﻿using CurrencyConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Helpers
{
	public abstract class ExchangeOperationProvider
	{
		public abstract ExchangeOperationResultVM ProvideExchange(
			List<KeyValuePair<string, double>> currencyExchangeRates, ExchangeOperationData exchangeData);
	}

	/// <summary>
	/// Provide exchange from left currency amount to right currency amount
	/// </summary>
	public class ExchangeOperationProviderForRightValueToSearch : ExchangeOperationProvider
	{
		//start
		public override ExchangeOperationResultVM ProvideExchange(
			List<KeyValuePair<string, double>> currencyExchangeRates, ExchangeOperationData exchangeData)
		{
			var firstRateVal = currencyExchangeRates.Where(i => i.Key == exchangeData.FirstCurrensyType).FirstOrDefault().Value;
			var secondRateVal = currencyExchangeRates.Where(i => i.Key == exchangeData.SecondCurrensyType).FirstOrDefault().Value;

			var convertedTo = (exchangeData.FirstCurrensyTypeAmount / firstRateVal) * secondRateVal;
			var rate = secondRateVal / firstRateVal;

			return new ExchangeOperationResultVM()
			{
				CurrencyFromType = exchangeData.FirstCurrensyType,
				CurrencyToType = exchangeData.SecondCurrensyType,
				ConvertedAmountResult = (decimal)convertedTo,
				ExchangeCourse = (decimal)rate
			};
		}
	}

	/// <summary>
	/// Provide exchange from right currency amount to left currency amount
	/// </summary>
	public class ExchangeOperationProviderForLeftValueToSearch : ExchangeOperationProvider
	{
		public override ExchangeOperationResultVM ProvideExchange(
			List<KeyValuePair<string, double>> currencyExchangeRates, ExchangeOperationData exchangeData)
		{
			var firstRateVal = currencyExchangeRates.Where(i => i.Key == exchangeData.SecondCurrensyType).FirstOrDefault().Value;
			var secondRateVal = currencyExchangeRates.Where(i => i.Key == exchangeData.FirstCurrensyType).FirstOrDefault().Value;

			var convertedTo = (exchangeData.SecondCurrensyTypeAmount / firstRateVal) * secondRateVal;
			var rate = secondRateVal / firstRateVal;

			return new ExchangeOperationResultVM()
			{
				CurrencyFromType = exchangeData.SecondCurrensyType,
				CurrencyToType = exchangeData.FirstCurrensyType, 
				ConvertedAmountResult = (decimal)convertedTo,
				ExchangeCourse = (decimal)rate
			};
		}
	}

	
}
