﻿using CurrencyConverter.Entities;
using CurrencyConverter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Helpers
{
	public class FilterManager
	{
		private IQueryable<ExchangeOperation> query;
		public FilterManager(IQueryable<ExchangeOperation> _query)
		{
			query = _query;
		}

		private DateTime GetDate(string date)
		{
			var day = int.Parse(date.Remove(2));
			var month = int.Parse(date.Substring(3, 2));
			var year = int.Parse(date[6..]); // int.Parse(date.Substring(6));

			return new DateTime(year, month, day);
		}

		public IQueryable<ExchangeOperation> GetFilteredExchangeOperationsList(ExchangeOperationFilterVM filter)
		{
			if (!string.IsNullOrEmpty(filter.FromCurrency))
				query = query.Where(x => x.FromCurrency.Contains(filter.FromCurrency));
			if (!string.IsNullOrEmpty(filter.ToCurrency))
				query = query.Where(x => x.ToCurrency.Contains(filter.ToCurrency));

			if (filter.PublishTimeSinceField != null && filter.PublishTimeTillField != null)
			{
				DateTime start_date = GetDate(filter.PublishTimeSinceField);
				DateTime finish_date = GetDate(filter.PublishTimeTillField);
				finish_date = finish_date.AddHours(23);
				finish_date = finish_date.AddMinutes(59);
				finish_date = finish_date.AddSeconds(59);

				query = query.Where(x => x.DateCreate >= start_date && x.DateCreate <= finish_date);
			}
			return query;
		}
	}
}
