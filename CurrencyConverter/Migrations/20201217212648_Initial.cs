﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace CurrencyConverter.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tblCurrencyTypes",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateModify = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateDelete = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblCurrencyTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tblExchangeOperationHistory",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FromCurrency = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    FromAmount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    ToCurrency = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    ToAmount = table.Column<decimal>(type: "decimal(18,4)", nullable: false),
                    DateCreate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateModify = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DateDelete = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tblExchangeOperationHistory", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "tblCurrencyTypes",
                columns: new[] { "Id", "DateCreate", "DateDelete", "DateModify", "Name" },
                values: new object[,]
                {
                    { 1, null, null, null, "USD" },
                    { 2, null, null, null, "EUR" },
                    { 3, null, null, null, "GBP" },
                    { 4, null, null, null, "CHF" }
                });

            migrationBuilder.InsertData(
                table: "tblExchangeOperationHistory",
                columns: new[] { "Id", "DateCreate", "DateDelete", "DateModify", "FromAmount", "FromCurrency", "ToAmount", "ToCurrency" },
                values: new object[] { 1, null, null, null, 1m, "USD", 1m, "USD" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tblCurrencyTypes");

            migrationBuilder.DropTable(
                name: "tblExchangeOperationHistory");
        }
    }
}
