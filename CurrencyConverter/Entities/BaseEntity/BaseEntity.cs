﻿using System;
using System.ComponentModel.DataAnnotations;


namespace CurrencyConverter.Entities.BaseEntity
{	
	public abstract class BaseEntity<T> : IBaseEntity<T>
	{
		[Key]
		public virtual T Id { get; set; }
		public virtual DateTime? DateCreate { get; set; }
		public virtual DateTime? DateModify { get; set; }
		public virtual DateTime? DateDelete { get; set; }
		
	}
}
