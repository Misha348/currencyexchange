﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CurrencyConverter.Entities
{
	public class AppDbContext : DbContext
	{
		public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
		{
		}
		public DbSet<CurrencyType> CurrencyTypes { get; set; }
		public DbSet<ExchangeOperation> ExchangeOperations { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
			base.OnModelCreating(modelBuilder);

			modelBuilder.Entity<ExchangeOperation>().HasData(
				new ExchangeOperation { Id = 1, FromCurrency = "USD", FromAmount = 1, ToCurrency = "USD", ToAmount = 1 });

			modelBuilder.Entity<CurrencyType>().HasData(
			 new CurrencyType { Id = 1, Name = "USD", IsSelected = true },
			 new CurrencyType { Id = 2, Name = "EUR", IsSelected = true },
			 new CurrencyType { Id = 3, Name = "GBP", IsSelected = true },
			 new CurrencyType { Id = 4, Name = "CHF", IsSelected = true },
			 new CurrencyType { Id = 5, Name = "CAD", IsSelected = false },
			 new CurrencyType { Id = 6, Name = "HKD", IsSelected = false },
			 new CurrencyType { Id = 7, Name = "DKK", IsSelected = false },
			 new CurrencyType { Id = 8, Name = "INR", IsSelected = false },
			 new CurrencyType { Id = 9, Name = "JPY", IsSelected = false },
			 new CurrencyType { Id = 10, Name = "THB", IsSelected = false },
			 new CurrencyType { Id = 11, Name = "CNY", IsSelected = false },
			 new CurrencyType { Id = 12, Name = "SGD", IsSelected = false },
			 new CurrencyType { Id = 13, Name = "AUD", IsSelected = false }, 
			 new CurrencyType { Id = 14, Name = "PLN", IsSelected = false },
			 new CurrencyType { Id = 15, Name = "ILS", IsSelected = false });

		}
    }
   
}
	
