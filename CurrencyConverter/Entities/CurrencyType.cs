﻿using CurrencyConverter.Entities.BaseEntity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CurrencyConverter.Entities
{
	[Table("tblCurrencyTypes")]
	public class CurrencyType: BaseEntity<int>
	{	
		[Required, StringLength(20)]
		public string Name { get; set; }

		public bool IsSelected { get; set; }
	}
}
