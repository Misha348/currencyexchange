﻿using System;
using CurrencyConverter.Entities.BaseEntity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CurrencyConverter.Entities
{
	[Table("tblExchangeOperationHistory")]
	public class ExchangeOperation: BaseEntity<int>
	{
		[Required, StringLength(20)]
		public string FromCurrency { get; set; }

		[Column(TypeName = "decimal(18,4)")]
		public decimal FromAmount { get; set; }

		[Required, StringLength(20)]
		public string ToCurrency { get; set; }

		[Column(TypeName = "decimal(18,4)")]
		public decimal ToAmount { get; set; }	
	}
}
